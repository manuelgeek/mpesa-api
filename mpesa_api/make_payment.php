<?php

/*Simplified MPESA API for PHP with lots and lots of comments with Explanations
*Incase you Need Extra Help feel free to tweet me '@jm_reed'
*Or drop me an email 'jamesrandom47@gmail.com'
*/

require_once('library/PayBillDetails.php');
require_once('library/MpesaAPI.php');

$password=Constant::generateHash();
$mClient=new MpesaAPI;
$transactionId= generateRandomString();

//Put the transaction details 

$callBackUrl=""; //Put the URL where you will Host the Receive File whihc is what Safaricom will callback once payment reaches your PayBill/Goes Through E.g http://your_domain_name/mpesa_api/receive_payment.php

$transactionDescription="Testing REED API"; //A short description that will show on the UUSD Prompt i.e "Testing REED API"

$transactionAmount="10";  //Put The Amount you want the user to pay. Minimum is 10Ksh.

$transactionPhoneNumber="";// Put The phone number you want payment to be made from  and it is in the format 254XXXXXXXXX i.e 254700000000

//Process The USSD Prompt on the provided Phone Number

$mClient->processCheckOutRequest($password,MERCHANT_ID,$transactionId,$transactionDescription,$transactionAmount,$transactionPhoneNumber,$callBackUrl);


//Function to Generate Your Own Random Unique Transaction ID. This is just your own Transaction ID and is NOT the MPESA Transaction ID. The MPESA Transaction ID will be provided by Safaricom to the CallBack url you specified

function generateRandomString() {
    $length = 10;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}



?>
