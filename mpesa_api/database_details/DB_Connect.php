<?php
class DB_Connect {

    // constructor
    function __construct() {
        
    }

    // destructor
    function __destruct() {
    }

    // Connecting to database
    public function connect() {
        // connecting to mysqli :Put values for your Username, Password and Database Name
		
        $con = mysqli_connect("localhost", "YOUR_USER_NAME", "YOUR_PASSWORD","mpesa_api");
        // selecting database
      

        // return database handler
        return $con;
    }

    // Closing database connection
    public function close() {
        mysqli_close();
    }

}

?>
