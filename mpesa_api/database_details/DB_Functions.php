<?php

class DB_Functions {

    private $db;

	

    //put your code here
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->db->connect();
		
		$connectionObject = new DB_Connect();

		$GLOBALS['con']=$connectionObject->connect();
		
    }

    // destructor
    function __destruct() {
        
    }

    /**
     * Storing new payment details
     * returns true if details are stored successfully
     */
	 public function storePaymentDetailsInDatabase($transactionId,$transactionPhoneNumber,$transactionAmount,$transactionDate,$transactionMpesaId)
		{
		
		 //Check if Transaction ID and MPESA ID already Exists in Database if Not then Insert
		$theQueryCheck = "SELECT * FROM mpesa_payments WHERE transactionId = '$transactionId' AND transactionMpesaId='$transactionMpesaId'";
		
		
		$resultCheck= mysqli_query($GLOBALS['con'],$theQueryCheck);
		
			 // check for result 
		$no_of_rows_check = mysqli_num_rows($resultCheck);
			if ($no_of_rows_check == 1 ) 
			{
				 //Already Exists so do not Add
			  return false;
			}
			
			else
			{
			
			//Doesnt Exists so Insert
			
			$result = mysqli_query($GLOBALS['con'],"INSERT INTO mpesa_payments(transactionId,transactionPhoneNumber,transactionAmount,transactionDate,transactionMpesaId) VALUES('$transactionId','$transactionPhoneNumber','$transactionAmount','$transactionDate','$transactionMpesaId')");
				
			if ($result)
			{
            // Insert was successful you can do something else here
            
			return true;		
			
			} 
			else 
			{
			 // Insert failed coz of probably some MYSQL error. Confirm connection details and trouble shoot
			return false;
			}

			
			}

         

				
		}
	

    /**
     * Check if payment exists in database or not
	 * Takes the Mpesa Transaction ID to confirm trancation.
	 *If the transaction exists it returns  an array with the Transaction Id, Phone NUmber, Amount, And Mpesa Transaction ID
     */
    public function isPaymentExistInDatabase($transactionMpesaId) {
      $result = mysqli_query($GLOBALS['con'],"SELECT transactionMpesaId from mpesa_payments WHERE transactionMpesaId = '$transactionMpesaId'");
      $response_array = array();

	   $no_of_rows = mysqli_num_rows($result);
        if ($no_of_rows > 0) {
            // payment exists in database 
			
			
			while($row = mysqli_fetch_array($result))

				{
					$response_array[0]= $row["transactionId"];
					$response_array[1] = $row["transactionPhoneNumber"];
					$response_array[2] = $row["transactionAmount"];
					$response_array[3] = $row["transactionMpesaId"];
					
				}
            return $response_array;
			
        } else 
		{
            // payment doesn't exist
            return false;
        }
    }

    
	
	

}

?>
