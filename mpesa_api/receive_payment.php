<?php

/*Simplified MPESA API for PHP with lots and lots of comments with Explanations
*Incase you Need Extra Help feel free to tweet me '@jm_reed'
*Or drop me an email 'jamesrandom47@gmail.com'
*/



//Process Callback after transaction

require_once 'database_details/DB_Functions.php'; 

$db= new DB_Functions();

$data = $_POST;

//Get POST Contents that have been sent by Safaricom once the payment got to the PayBill



	$transactionId                = $data["TRX_ID"];
	$transactionPhoneNumber       = $data["MSISDN"];
	$transactionAmount            = $data["AMOUNT"];
	$transactionStatus            = $data["TRX_STATUS"];
	$transactionReturnCode        = $data["RETURN_CODE"];
	$transactionDate              = $data["MPESA_TRX_DATE"];
	$transactionMpesaId           = $data["MPESA_TRX_ID"];
	

	//Save the returned data into the database or use it to finish certain operation i.e Send SMS notification or Activate App or Service

	if($transactionStatus == "Success")
	{
		
		//Payment was successfully made and erecived so Insert Details to your Database
		
		$result = $db->storePaymentDetailsInDatabase($transactionId,$transactionPhoneNumber,$transactionAmount,$transactionDate,$transactionMpesaId);
		
		if($result)
		{
		//Data was successfully inserted to your database. Do something here like SEND User an SMS or Activate a Service
		
		}
		
		else
		{
		
		//Data was not inserted to your database.

        //Probably a MYSQL Error. Check your database connections details
		
		}

	}
	else
	{
		//Transaction Failed.
		
		//Check the Return Code and Figure out why Payment did not succeed. A successfull payment has code '00'. View the different error codes on the MPESA API Docs to help you troubleshoot

	}



?>